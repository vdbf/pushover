<?php

namespace Vdbf\Component\Pushover\Tests;

use Vdbf\Component\Pushover\Pushover;
use Vdbf\Component\Pushover\Message;
use Vdbf\Component\Pushover\Request;
use Vdbf\Component\Pushover\Response;
use \Mockery;

class PushoverTest extends \PHPUnit_Framework_TestCase
{
	
	public function testTokenConstruct()
	{
		$pushover = new Pushover('secrettoken', new Request, new \SplObjectStorage);
		$this->assertEquals('secrettoken', $pushover->getToken());
	}

	public function testSettersAndGetters()
	{
		$tokenAlternative = 'notsecrettoken';
		$requestAlternative = new Request;
		$messagesAlternative = new \SplObjectStorage;

		$pushover = new Pushover();
		$pushover->setToken($tokenAlternative);
		$pushover->setRequest($requestAlternative);
		$pushover->setMessages($messagesAlternative);

		$this->assertEquals($tokenAlternative, $pushover->getToken());
		$this->assertEquals($requestAlternative, $pushover->getRequest());
		$this->assertEquals($messagesAlternative, $pushover->getMessages());
	}

	public function testSend()
	{
		$request = Mockery::mock('Vdbf\\Component\\Pushover\\Request[doRequest,setData]');
		$request->shouldReceive('setData')->times(1);
		$request->shouldReceive('doRequest')->times(1);

		$message = new Message('usertoken', 'Hello world!');	
		$pushover = new Pushover('secrettoken', $request);

		$pushover->addMessage($message);

		$pushover->send();
	}

}