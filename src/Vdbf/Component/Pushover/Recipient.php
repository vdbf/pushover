<?php

namespace Vdbf\Component\Pushover;

class Recipient
{

	protected $key;
	protected $devices;

	public function getKey()
	{
		return $this->key;
	}

	public function setKey($key)
	{
		$this->key = $key;
		return $this;
	}

	public function getDevice()
	{
		return $this->device;
	}

	public function setDevice($device)
	{
		$this->device = $device;
	}

}
