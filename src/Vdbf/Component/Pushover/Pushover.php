<?php

namespace Vdbf\Component\Pushover;

use Vdbf\Component\Pushover\Message;
use Vdbf\Component\Pushover\Request;
use \SplObjectStorage;

class Pushover
{

	protected $token;
	protected $messages;
	protected $request;

	/**
	 * Construct a pushover object and set dependencies
	 * @param $token
	 * @param Vdbf\Component\Pushover\Request $request
	 * @param SplObjectStorage $messages;
	 * @author Eelke van den Bos <eelkevdbos@gmail.com>
	 */
	public function __construct($token = null, Request $request = null, SplObjectStorage $messages = null)
	{
		$this->token = $token;
		$this->request = $request ?: new Request;
		$this->messages = $messages ?: new SplObjectStorage;
	}

	public function getToken()
	{
		return $this->token;
	}

	public function setToken($token)
	{
		$this->token = $token;
		return $this;
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function setRequest(Request $request)
	{
		$this->request = $request;
		return $this;
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function addMessage(Message $message)
	{
		$this->messages->attach($message);
		return $this;
	}

	public function setMessages(SplObjectStorage $messages)
	{
		$this->messages = $messages;
		return $this;
	}

	public function send()
	{
		$result = array();
		$request = $this->request;
		$messages = $this->messages;

		foreach($messages as $message) {

			$data = $message->toArray();
			$data['token'] = $this->getToken();

			$request->setData($data);

			$result[] = $request->doRequest();

		}

		return $result;		
	}

}