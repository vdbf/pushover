<?php

namespace Vdbf\Component\Pushover;

use \SplObjectStorage;

class Message
{

	protected $user;
	protected $message;
	protected $title;
	protected $url;
	protected $url_title;
	protected $priority;
	protected $timestamp;
	protected $sound;

	const PRIORITY_LOW = -1;
	const PRIORITY_NORMAL = 0;
	const PRIORITY_HIGH = 1;
	const PRIORITY_EMERGENCY = 2;

	public function __construct($user = '', $message = '', $title = '', $url = '', $urlTitle = '', $priority = 0, $timestamp = null, $sound = 'default')
	{
		$this->user = $user;
		$this->message = $message;
		$this->title = $title;
		$this->url = $url;
		$this->url_title = $urlTitle;
		$this->priority = $priority;
		$this->timestamp = $timestamp ?: time();
		$this->sound = $sound;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getUrl()
	{
		return $this->url;
	}

	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	public function getUrlTitle()
	{
		return $this->url_title;
	}

	public function setUrlTitle($urlTitle)
	{
		$this->url_title = $urlTitle;
	}

	public function getPriority()
	{
		return $this->priority;
	}

	public function setPriority($priority)
	{
		$this->priority = $priority;
		return $this;
	}

	public function getTimestamp()
	{
		return $this->timestamp;
	}

	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
		return $this;
	}

	public function getSound()
	{
		return $this->sound;
	}

	public function setSound($sound)
	{
		$this->sound = $sound;
		return $this;
	}

	public function toArray($excludes = array())
	{
		$output = array();
		foreach($this as $key => $value)
		{
			if(!in_array($key, $excludes)) {
				$output[$key] = $value;
			}
		}
		return $output;
	}

}