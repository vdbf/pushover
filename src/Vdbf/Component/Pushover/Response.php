<?php

namespace Vdbf\Component\Pushover;

class Response
{

	protected $header;
	protected $info;
	protected $status;

	const STATUS_OK = 200;
	const STATUS_REJECTED = 429;
	const STATUS_UNAVAILABLE = 500;

	public function getHeader()
	{
		return $this->header;
	}

	public function setHeader($header)
	{
		$this->header = $header;
		return $this;
	}

	public function getInfo()
	{
		return $this->info;
	}

	public function setInfo($info)
	{
		$this->info = $info;
		return $this;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	public function reset()
	{
		$this->header = null;
		$this->info = null;
		$this->status = null;
	}

}