<?php

namespace Vdbf\Component\Pushover;

use Vdbf\Component\Pushover\Response;

class Request
{

	protected $response;
	protected $data;

	public function __construct(Response $response = null)
	{
		$this->response = $response ?: new Response;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function doRequest()
	{
		$data = $this->data;
		$response = clone $this->response;

		try {

			curl_setopt_array($ch = curl_init(), array(
				CURLOPT_URL => "https://api.pushover.net/1/messages.json",
				CURLOPT_POSTFIELDS => $data,
				CURLOPT_RETURNTRANSFER => true,
				//CURLOPT_NOBODY => true
				)
			);

			$header = curl_exec($ch);
			$info = curl_getinfo($ch);

			curl_close($ch);

			$response->setHeader($header);
			$response->setInfo($info);

		} catch (Exception $e) {

			throw new Exception('Curl request to pushover api could not be completed.');
		}

		return $response;
	}



}